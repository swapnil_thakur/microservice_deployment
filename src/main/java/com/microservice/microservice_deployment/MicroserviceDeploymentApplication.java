package com.microservice.microservice_deployment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = {"com.microservice.*"})
@ComponentScan(basePackages = "com.microservice.*")
public class MicroserviceDeploymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceDeploymentApplication.class, args);
    }

}
