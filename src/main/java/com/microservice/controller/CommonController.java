package com.microservice.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {

    @RequestMapping(value = "/getStatus", method = RequestMethod.GET)
    public ResponseEntity<String> getStatus() {
        return new ResponseEntity<String>("{Status:UP}", HttpStatus.OK);
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public ResponseEntity<String> getHealth() {
        return new ResponseEntity<String>("{Status:UP}", HttpStatus.OK);
    }

    @RequestMapping(value = "/getHealth", method = RequestMethod.GET)
    public ResponseEntity<String> getHealthInfo() {
        return new ResponseEntity<String>("{Status:UP}", HttpStatus.OK);
    }

    @RequestMapping(value = "/getInformation", method = RequestMethod.GET)
    public ResponseEntity<String> getInformation() {
        return new ResponseEntity<String>("{Status:UP}", HttpStatus.OK);
    }

    @RequestMapping(value = "/getInformation1", method = RequestMethod.GET)
    public ResponseEntity<String> getInformation1() {
        return new ResponseEntity<String>("{Status:UP}", HttpStatus.OK);
    }

    @RequestMapping(value = "/getInformation2", method = RequestMethod.GET)
    public ResponseEntity<String> getInformation2() {
        return new ResponseEntity<String>("{Status:UP}", HttpStatus.OK);
    }

    @RequestMapping(value = "/getInformation5", method = RequestMethod.GET)
    public ResponseEntity<String> getInformation6() {
        return new ResponseEntity<String>("{Status:UP}", HttpStatus.OK);
    }
}
