FROM openjdk:8-jdk-alpine

COPY target/microservice_deployment.jar microservice_deployment.jar

EXPOSE 8083

ENTRYPOINT ["java","-jar","/microservice_deployment.jar"]
